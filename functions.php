<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
      
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_separate', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array() );
        //wp_enqueue_script( 'script-name', trailingslashit( get_stylesheet_directory_uri() ) . 'theme.js', array(), '1.0.0', true );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 20 );


/**
 * Enable features from Soil when plugin is activated
 * @link https://roots.io/plugins/soil/
 */
add_theme_support('soil-clean-up');
add_theme_support('soil-disable-asset-versioning');
add_theme_support('soil-disable-trackbacks');
//add_theme_support('soil-google-analytics', 'UA-XXXXX-Y');
//add_theme_support('soil-jquery-cdn');
//add_theme_support('soil-js-to-footer');
add_theme_support('soil-nav-walker');
add_theme_support('soil-nice-search');
add_theme_support('soil-relative-urls');


/* Disable WP Notifications for all except superadmin */
function hide_update_notice_to_all_but_superadmin_user() {
	if ( !is_super_admin() ) {
		remove_action( 'admin_notices', 'update_nag', 3 );
	}
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_superadmin_user', 1 );

// Custom Login Logo
function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(/wp-content/uploads/2019/02/logo-black.svg) !important; width: 65px !important; background-size: 65px !important; height: 65px !important; }
    </style>';
}
add_action('login_head', 'custom_login_logo');

// Change Logo Link
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'https://sylviajagla.com/cms';
}

function my_login_logo_url_title() {
    return 'Sylvia Jagla';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


// Show Podcasts on author page
function post_types_author_archives($query) {
    if ($query->is_author)
      // Add 'podcasts' CPT and the default 'posts' to display in author's archive
      $query->set( 'post_type', array('podcast', 'posts') );
      remove_action( 'pre_get_posts', 'custom_post_author_archive' );
}
add_action('pre_get_posts', 'post_types_author_archives');

function namespace_insert_before_first_paragraph( $content ) { 

    if ( is_singular( 'podcast' ) ) {
      $my_div = '<div class="podcast-links"><span class="itunes-link">[pods field="itunes_link"]</span> <span class="soundcloud-link">[pods field="soundcloud_link"]</span> <span class="spotify-link">[pods field="spotify_link"]</span></div>';
      $first_para_pos = strpos( $content, '<aside' );
      $new_content = substr_replace( $content, $my_div, $first_para_pos, 0 );

      return $new_content;
    }

    else {
      return $content;
    }
        
}

add_filter( 'the_content', 'namespace_insert_before_first_paragraph' );

